		<?php if ( is_front_page() ) : get_template_part( 'part', 'block-1' ); endif; ?>
		<?php get_template_part( 'part', 'block-2' ); ?>
		<?php get_template_part( 'part', 'block-3' ); ?>
		<?php get_template_part( 'part', 'bottom' ); ?>
		<?php get_template_part( 'part', 'copyright' ); ?>
		<?php wp_footer(); ?>
	</body>
</html>